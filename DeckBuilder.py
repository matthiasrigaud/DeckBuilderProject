#!/usr/bin/env python3

##
## Main DeckBuilder file
##

import pygame
import time
import sys

sys.path.append('.')

from src import Constant, Cards_utils, Init, Collection, Sort, Event, Graphical

if __name__ == '__main__':

    ##
    ## Init values
    ##

    pygame.init()
    screen = pygame.display.set_mode((Constant.WIN_WIDTH, Constant.WIN_HEIGHT))
    screen.fill(Constant.WIN_BACKGROUND)
    screen.blit(pygame.font.Font(Constant.FONT_FILE, 50).render("Loading ...", True, [50, 50, 50]),
                [(Constant.WIN_WIDTH - 200) / 2, (Constant.WIN_HEIGHT - 100) / 2])
    pygame.display.set_caption(Constant.WIN_TITLE)
    pygame.display.flip()
    pygame.key.set_repeat(500, 50)
    cards_data = Cards_utils.load_cards(Constant.EXTS_FILE)
    objects, window_filter = Init.init_objects()
    cursor = 0
    cardfilter = {"CATEGORY": ["Pokemon"], "OWNED": True, "NOT OWNED": True}
    is_in_zoom = False
    draw = True
    collection = Collection.Collection()
    sort_criter = "NAME"
    cards_data = Sort.sort_cards(cards_data, sort_criter)
    filtered_cards = Cards_utils.search_cards(cards_data, cardfilter, collection)


    ## Main loop
    while 1:

        ## get the quantity of cur_card actually in collection
        if len(filtered_cards) > 0:
            nb_card = collection.get_nb_cards(filtered_cards[cursor])

        ## Save values before get event
        oldcursor = cursor
        old_sort_criter = sort_criter
        old_nb_card = nb_card
        old_is_in_zoom = is_in_zoom

        ## Call event manager
        cursor, is_in_zoom, cardfilter, have_filter_change, nb_card, sort_criter, draw \
            = Event.manager(cursor, len(filtered_cards), is_in_zoom, objects, cardfilter, nb_card, sort_criter, draw, window_filter)

        ## set the quantity of cur_card in collection
        if oldcursor == cursor and len(filtered_cards) > 0:
            collection.set_nb_cards(filtered_cards[cursor], nb_card)

        ## if the sort mode have changed, get the new card order
        if old_sort_criter != sort_criter or (sort_criter == "QTTY" and oldcursor == cursor and old_nb_card != nb_card):
            cards_data = Sort.sort_cards(cards_data, sort_criter)
            have_filter_change = True

        ##
        ## if something have changer in the card order or filter, get the new card array
        ##
        if have_filter_change:
            window_filter.change_mode(cardfilter["CATEGORY"][0])
            if objects["FILTER TOGGLE"].get_state() == 0:
                window_filter.change_filter(cardfilter)
            objects["SORT MENU"].change_object(cardfilter["CATEGORY"][0]);
            if sort_criter != objects["SORT MENU"].get_active_element():
                sort_criter = objects["SORT MENU"].get_active_element()
                cards_data = Sort.sort_cards(cards_data, sort_criter)
            filtered_cards = Cards_utils.search_cards(cards_data, cardfilter, collection)
            if cursor >= len(filtered_cards) and len(filtered_cards) > 0:
                cursor = len(filtered_cards) - 1
            elif len(filtered_cards) == 0:
                cursor = 0

        ##
        ## call draw functions
        ##
        if draw or oldcursor != cursor or have_filter_change or old_is_in_zoom != is_in_zoom:
            screen.fill(Constant.WIN_BACKGROUND)
            draw = True
        if not is_in_zoom or draw:
            Graphical.draw_toolbar(screen, objects)
        if draw:
            Graphical.scrolled_cards_displayer(screen, filtered_cards, cursor, is_in_zoom);
            draw = False
        if not is_in_zoom and len(filtered_cards) > 0:
            Graphical.draw_interface(screen, objects["PLUS"], objects["MINUS"], nb_card)
        window_filter.draw(screen)
        pygame.display.flip()
        time.sleep(0.01)

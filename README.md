# DeckBuilderProject

![Insert Image](https://gitlab.com/uploads/project/avatar/2374531/pokemonTCG.png)

DeckBuilderProject have for goal to make a Deck Builder for the Pokemon TCG.

## Installation :

### Required :
- Python3.*
  - For Linux : use your package manager (recommended) or direct download [here](https://www.python.org/downloads/source/)
  - For Window : direct download [3.2.5 installer](https://www.python.org/ftp/python/3.2.5/python-3.2.5.msi) (recommended) or choose your version [here](https://www.python.org/downloads/windows/)
- Pygame
  - For Linux : using Python Package Index `sudo pip install pygame` (recommended) or directly on [pygame download page](http://www.pygame.org/download.shtml)
  - For Window : direct download [installer for python3.2](http://pygame.org/ftp/pygame-1.9.2a0.win32-py3.2.msi) (recommended) or directly on [pygame download page](http://www.pygame.org/download.shtml)

For Linux, an `Install.sh` script is provided. You can then launch the Deck Builder typing `pkmnDeckBuilder`.
import json
import os
import sys

from . import Constant

##
## Collection manager
##

class Collection:

    def __init__(self):
        try:
            self.collection_file = open(Constant.COLLECTION_FILE, "r+")
            try:
                self.collection = json.load(self.collection_file)
            except ValueError:
                self.collection = {}
                self.collection_file.close()
        except IOError:
            self.collection = {}


    ##
    ## Return how many copies of cur_card are in Collection
    ##
    def get_nb_cards(self, cur_card):
        try:
            nb_cards = self.collection[cur_card["EXTENSION"]][str(cur_card["NUMBER"])]
        except KeyError:
            nb_cards = 0
        return nb_cards


    ##
    ## take and save the quantity of copies of cur_card in collection
    ##
    def set_nb_cards(self, cur_card, nb_cards):
        if ((cur_card["EXTENSION"] not in self.collection or str(cur_card["NUMBER"]) not in self.collection[cur_card["EXTENSION"]])
            and not nb_cards):
            return
        if cur_card["EXTENSION"] not in self.collection:
            self.collection[cur_card["EXTENSION"]] = {}
        if nb_cards:
            self.collection[cur_card["EXTENSION"]][str(cur_card["NUMBER"])] = nb_cards
        else:
            del self.collection[cur_card["EXTENSION"]][str(cur_card["NUMBER"])]
            if len(self.collection[cur_card["EXTENSION"]]) == 0:
                del self.collection[cur_card["EXTENSION"]]
                if len(self.collection) == 0:
                    os.remove(self.collection_file.name)
                    return
        if not os.path.isdir(Constant.LOCAL_DIR):
            try:
                os.mkdir(Constant.LOCAL_DIR)
            except:
                print("ERROR : Can't create " + Constant.LOCAL_DIR, file=sys.stderr)
                sys.exit()
        self.collection_file = open(Constant.COLLECTION_FILE, "w+")
        json.dump(self.collection, self.collection_file)
        self.collection_file.close()

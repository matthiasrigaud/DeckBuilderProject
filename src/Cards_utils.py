##
## Contain function to manage cards
##

import re
import json

from . import Constant

##
## return all cards presents in cards_data that matches criters
##

def search_cards(cards_data, criters, collection):
    matched_cards = []
    for field in iter(criters):
        for i_card in range(0, len(cards_data)):
            if field == "OWNED":
                if ((criters[field] and (collection.get_nb_cards(cards_data[i_card]) > 0 or criters["NOT OWNED"]))
                    or not criters[field] and collection.get_nb_cards(cards_data[i_card]) == 0):
                    matched_cards.append(cards_data[i_card])
                continue
            if field == "NOT OWNED":
                if ((criters[field] and (collection.get_nb_cards(cards_data[i_card]) == 0 or criters["OWNED"]))
                    or not criters[field] and collection.get_nb_cards(cards_data[i_card]) > 0):
                    matched_cards.append(cards_data[i_card])
                continue
            for i_field in range(0, len(criters[field])):
                try:
                    if re.search(".*" + criters[field][i_field] + ".*", cards_data[i_card][field], re.IGNORECASE):
                        matched_cards.append(cards_data[i_card])
                        break
                except :
                    continue
        cards_data = matched_cards
        matched_cards = []
    return cards_data

##
## load cards data
##
def load_cards(extensions_file):
    extensions = open(extensions_file);
    extensions_data = json.load(extensions)
    cards_json = json.load(open(Constant.PKMN_FILE))
    cards_data = [];

    for i in range(len(cards_json["DATA"])):
        cards_data.append(cards_json["DATA"][i]);
        cards_data[i]["EXTENSION_DATA"] = extensions_data[cards_json["DATA"][i]["EXTENSION"]]
    return cards_data

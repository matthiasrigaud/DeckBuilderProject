import pygame
import os

####### CONSTANT GLOBAL VALUES ########

DB_DIR                    = "./TCG-DataBase/"
HOME                      = os.environ.get("HOME")

PKMN_FILE                 = DB_DIR + "pokemon.json"
EXTS_FILE                 = DB_DIR + "extensions.json"
TYPE_DIR                  = DB_DIR + "/types/"
FONT_DIR                  = DB_DIR + "/font/"
LOCAL_DIR                 = HOME + "/.pkmnDeckBuilder/"

WIN_TITLE                 = "Ninjachu's Deck Manager"
WIN_HEIGHT                = 1000
WIN_WIDTH                 = 1600
WIN_BACKGROUND            = [187, 210, 225]

NEXT_CARD_PERCENT_SIZE    = 0.9
NEXT_CARD_PERCENT_X       = 0.4
NEXT_CARD_ALPHA           = 200
EDGE_CARD_PERCENT_SIZE    = 0.8
EDGE_CARD_PERCENT_X       = 0.8
EDGE_CARD_ALPHA           = 100
MIDDLE_CARD_HEIGHT        = 570
MIDDLE_CARD_WIDTH         = 409
X_MIDDLE_CARD             = (WIN_WIDTH - MIDDLE_CARD_WIDTH) / 2
Y_MIDDLE_CARD             = 0.95 * WIN_HEIGHT - MIDDLE_CARD_HEIGHT

TOOLBAR_COLOR             = [157, 180, 195]
TOOLBAR_WIDTH             = 1600
TOOLBAR_HEIGHT            = 300
FONT_FILE                 = FONT_DIR + "CODE Bold.otf"

COLLECTION_FILE           = LOCAL_DIR + "collection.json"

KEY_CHAR                  = {pygame.K_SPACE:              " ",
                             pygame.K_EXCLAIM:            "!",
                             pygame.K_QUOTEDBL:           "\"",
                             pygame.K_HASH:               "#",
                             pygame.K_DOLLAR:             "$",
                             pygame.K_AMPERSAND:          "&",
                             pygame.K_LEFTPAREN:          "(",
                             pygame.K_RIGHTPAREN:         ")",
                             pygame.K_ASTERISK:           "*",
                             pygame.K_PLUS:               "+",
                             pygame.K_COMMA:              ",",
                             pygame.K_MINUS:              "-",
                             pygame.K_PERIOD:             ".",
                             pygame.K_SLASH:              "/",
                             pygame.K_COLON:              ":",
                             pygame.K_SEMICOLON:          ";",
                             pygame.K_LESS:               "<",
                             pygame.K_EQUALS:             "=",
                             pygame.K_GREATER:            ">",
                             pygame.K_QUESTION:           "?",
                             pygame.K_AT:                 "@",
                             pygame.K_LEFTBRACKET:        "[",
                             pygame.K_BACKSLASH:          "\\",
                             pygame.K_RIGHTBRACKET:       "]",
                             pygame.K_CARET:              "^",
                             pygame.K_UNDERSCORE:         "_",
                             pygame.K_BACKQUOTE:          "`",
                             pygame.K_0:                  "0",
                             pygame.K_1:                  "1",
                             pygame.K_2:                  "2",
                             pygame.K_3:                  "3",
                             pygame.K_4:                  "4",
                             pygame.K_5:                  "5",
                             pygame.K_6:                  "6",
                             pygame.K_7:                  "7",
                             pygame.K_8:                  "8",
                             pygame.K_9:                  "9",
                             pygame.K_a:                  "a",
                             pygame.K_b:                  "b",
                             pygame.K_c:                  "c",
                             pygame.K_d:                  "d",
                             pygame.K_e:                  "e",
                             pygame.K_f:                  "f",
                             pygame.K_g:                  "g",
                             pygame.K_h:                  "h",
                             pygame.K_i:                  "i",
                             pygame.K_j:                  "j",
                             pygame.K_k:                  "k",
                             pygame.K_l:                  "l",
                             pygame.K_m:                  "m",
                             pygame.K_n:                  "n",
                             pygame.K_o:                  "o",
                             pygame.K_p:                  "p",
                             pygame.K_q:                  "q",
                             pygame.K_r:                  "r",
                             pygame.K_s:                  "s",
                             pygame.K_t:                  "t",
                             pygame.K_u:                  "u",
                             pygame.K_v:                  "v",
                             pygame.K_w:                  "w",
                             pygame.K_x:                  "x",
                             pygame.K_y:                  "y",
                             pygame.K_z:                  "z"}

WIN_FILTER_VALUES         = {"Fire":                    "TYPE",
                             "Fairy":                   "TYPE",
                             "Fighting":                "TYPE",
                             "Lightning":               "TYPE",
                             "Metal":                   "TYPE",
                             "Water":                   "TYPE",
                             "Dragon":                  "TYPE",
                             "Colorless":               "TYPE",
                             "Darkness":                "TYPE",
                             "Psychic":                 "TYPE",
                             "Grass":                   "TYPE",
                             "BREAK":                   "SUBCATEGORY",
                             "Restored":                "SUBCATEGORY",
                             "Basic":                   "SUBCATEGORY",
                             "Stage 2":                 "SUBCATEGORY",
                             "Level X":                 "SUBCATEGORY",
                             "Stage 1":                 "SUBCATEGORY",
                             "Item":                    "SUBCATEGORY",
                             "Tool":                    "SUBCATEGORY",
                             "Stadium":                 "SUBCATEGORY",
                             "Basic":                   "SUBCATEGORY",
                             "Special":                 "SUBCATEGORY",
                             "Supporter":               "SUBCATEGORY",
                             "Technical Machine":       "SUBCATEGORY",
                             "EX":                      "ATTRIBUTE",
                             "Mega":                    "ATTRIBUTE",
                             "Legend":                  "ATTRIBUTE"}

FILTER_CATEGORIES         = ["TYPE",
                             "SUBCATEGORY",
                             "ATTRIBUTE"]

#######################################

from . import User_interface

##
## C++ interface-like for user-interface object
##
class Interface_box(object):

    def __init__(self, object_dict, current_object):
        self.local_dict = object_dict
        self.current_object = current_object

    def change_object(self, new_object_name):
        self.current_object = new_object_name

    def set_state(self, state):
        return self.local_dict[self.current_object].set_state(state)

    def get_state(self):
        return self.local_dict[self.current_object].get_state()

    def is_in_toolbar(self):
        return self.local_dict[self.current_object].is_in_toolbar()

    def is_overflew(self):
        return self.local_dict[self.current_object].is_overflew()

    def draw(self, screen):
        return self.local_dict[self.current_object].draw(screen)

    def get_str(self):
        return self.local_dict[self.current_object].get_str()

    def edit_str(self, key):
        return self.local_dict[self.current_object].edit_str(key)

    def is_dropped(self):
        return self.local_dict[self.current_object].is_dropped()

    def change_active_element(self):
        return self.local_dict[self.current_object].change_active_element()

    def toggle(self):
        return self.local_dict[self.current_object].toggle()

    def get_active_element(self):
        return self.local_dict[self.current_object].get_active_element()

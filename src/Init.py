##
## contain init functions
##

from . import Constant, User_interface, Object_box

##
## Init interface objects
##

def init_objects():
    objects = {}
    sort_menu = {}
    filter_objects = {}

    ##           ##
    ## MAIN TABS ##
    ##           ##

    objects["COLLECTION"] = User_interface.Button(Constant.FONT_FILE, [800, 80], [0, 0], 0,
                                                  [[157, 180, 195], [187, 210, 225], [170, 190, 205]],
                                                  {"TXT": "COLLECTION", "SIZE": 28,
                                                   "COLOR": [[250, 250, 250], [200, 200, 200], [150, 150, 150]]}, True)
    objects["DECK"] = User_interface.Button(Constant.FONT_FILE, [800, 80], [800, 0], 2,
                                            [[157, 180, 195], [187, 210, 225], [170, 190, 205]],
                                            {"TXT": "DECK", "SIZE": 28,
                                             "COLOR": [[250, 250, 250], [200, 200, 200], [150, 150, 150]]}, True)

    ##          ##
    ## SUB TABS ##
    ##          ##

    objects["Pokemon"] = User_interface.Button(Constant.FONT_FILE, [200, 70], [0, 230], 0,
                                               [[187, 210, 225], [207, 230, 245], [157, 180, 195]],
                                               {"TXT": "Pokemon", "SIZE": 25,
                                                "COLOR": [[250, 250, 250], [200, 200, 200], [150, 150, 150]]}, True)
    objects["Trainer"] = User_interface.Button(Constant.FONT_FILE, [200, 70], [200, 230], 2,
                                               [[187, 210, 225], [207, 230, 245], [157, 180, 195]],
                                               {"TXT": "Trainer", "SIZE": 25,
                                                "COLOR": [[250, 250, 250], [200, 200, 200], [150, 150, 150]]}, True)
    objects["Energy"] = User_interface.Button(Constant.FONT_FILE, [200, 70], [400, 230], 2,
                                              [[187, 210, 225], [207, 230, 245], [157, 180, 195]],
                                              {"TXT": "Energy", "SIZE": 25,
                                               "COLOR": [[250, 250, 250], [200, 200, 200], [150, 150, 150]]}, True)

    ##                 ##
    ## CARDS INTERFACE ##
    ##                 ##

    objects["TO BEGIN"] = User_interface.Button(Constant.FONT_DIR + "Ubuntu-Title.ttf", [40, 50],
                                                [10, Constant.Y_MIDDLE_CARD + (Constant.MIDDLE_CARD_HEIGHT - 50) / 2], 2,
                                                [[], [71, 181, 255], [71, 151, 205]],
                                                {"TXT": "<<", "SIZE": 30,
                                                 "COLOR": [[], [255, 255, 255], [230, 230, 230]]}, True)
    objects["TO END"] = User_interface.Button(Constant.FONT_DIR + "Ubuntu-Title.ttf", [40, 50],
                                              [Constant.WIN_WIDTH - 50, Constant.Y_MIDDLE_CARD + (Constant.MIDDLE_CARD_HEIGHT - 50) / 2], 2,
                                              [[], [71, 181, 255], [71, 151, 205]],
                                              {"TXT": ">>", "SIZE": 30,
                                               "COLOR": [[], [255, 255, 255], [230, 230, 230]]}, True)
    objects["BACKWARD"] = User_interface.Button(Constant.FONT_DIR + "Ubuntu-Title.ttf", [40, 50],
                                                [60, Constant.Y_MIDDLE_CARD + (Constant.MIDDLE_CARD_HEIGHT - 50) / 2], 2,
                                                [[], [181, 235, 71], [181, 215, 71]],
                                                {"TXT": "<", "SIZE": 30,
                                                 "COLOR": [[], [255, 255, 255], [230, 230, 230]]}, True)
    objects["FORWARD"] = User_interface.Button(Constant.FONT_DIR + "Ubuntu-Title.ttf", [30, 50],
                                               [Constant.WIN_WIDTH - 90, Constant.Y_MIDDLE_CARD +
                                                (Constant.MIDDLE_CARD_HEIGHT - 50) / 2], 2,
                                               [[], [181, 235, 71], [181, 215, 71]],
                                               {"TXT": ">", "SIZE": 30,
                                                "COLOR": [[], [255, 255, 255], [230, 230, 230]]}, True)
    objects["PLUS"] = User_interface.Button(Constant.FONT_DIR + "Ubuntu-Title.ttf", [40, 40],
                                            [Constant.X_MIDDLE_CARD + Constant.MIDDLE_CARD_WIDTH - 20, Constant.Y_MIDDLE_CARD + 100], 2,
                                            [[], [71, 235, 71], [71, 215, 71]],
                                            {"TXT": "+", "SIZE": 30,
                                             "COLOR": [[], [255, 255, 255], [230, 230, 230]]}, False)
    objects["MINUS"] = User_interface.Button(Constant.FONT_DIR + "Ubuntu-Title.ttf", [40, 40],
                                             [Constant.X_MIDDLE_CARD + Constant.MIDDLE_CARD_WIDTH - 20, Constant.Y_MIDDLE_CARD + 150], 2,
                                             [[], [235, 71, 71], [215, 71, 71]],
                                             {"TXT": "-", "SIZE": 30,
                                              "COLOR": [[], [255, 255, 255], [230, 230, 230]]}, False)

    ##                        ##
    ## OWNED/NOT OWNED SWITCH ##
    ##                        ##

    objects["OWNED"] = User_interface.Check_box(Constant.FONT_FILE, [20, 20], [850, 150], 0,
                                                [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                {"TXT": "Show owned", "SIZE": 20,
                                                 "COLOR": [0, 0, 0]}, [250, 250, 250], 2, True)
    objects["NOT OWNED"] = User_interface.Check_box(Constant.FONT_FILE, [20, 20], [850, 200], 0,
                                                    [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                    {"TXT": "Show not owned", "SIZE": 20,
                                                     "COLOR": [0, 0, 0]}, [250, 250, 250], 2, True)

    ##            ##
    ## SEARCH BOX ##
    ##            ##

    objects["SEARCH BOX"] = User_interface.Text_box(Constant.FONT_DIR + "Ubuntu-Title.ttf", [200, 36], [1200, 150], 2,
                                                    [[250, 250, 250], [200, 200, 200], [150, 150, 150]],
                                                    {"TXT": "Search", "SIZE": 20,
                                                     "COLOR": [[0, 0, 0], [0, 0, 0], [0, 0, 0]]}, True)

    ##           ##
    ## SORT MENU ##
    ##           ##

    sort_menu["Pokemon"] = User_interface.Drop_menu(Constant.FONT_FILE, [180, 30],
                                                    [600, 150], 2,
                                                    [[200, 200, 240], [170, 170, 210], [150, 150, 190]],
                                                    {"TXT": "Sort by :", "SIZE": 20,
                                                     "COLOR": [[255, 255, 255], [230, 230, 230], [210, 210, 210]]}, True,
                                                    {"QTTY": "Quantity", "NAME": "Name", "STAGE": "Stage",
                                                     "TYPE": "Type", "EXT": "Extension", "FML": "Family"}, "NAME")
    sort_menu["Energy"] = User_interface.Drop_menu(Constant.FONT_FILE, [180, 30],
                                                   [600, 150], 2,
                                                   [[200, 200, 240], [170, 170, 210], [150, 150, 190]],
                                                   {"TXT": "Sort by :", "SIZE": 20,
                                                    "COLOR": [[255, 255, 255], [230, 230, 230], [210, 210, 210]]}, True,
                                                   {"QTTY": "Quantity", "NAME": "Name", "NRJCAT": "Category",
                                                    "TYPE": "Type", "EXT": "Extension"}, "NAME")
    sort_menu["Trainer"] = User_interface.Drop_menu(Constant.FONT_FILE, [180, 30],
                                                    [600, 150], 2,
                                                    [[200, 200, 240], [170, 170, 210], [150, 150, 190]],
                                                    {"TXT": "Sort by :", "SIZE": 20,
                                                     "COLOR": [[255, 255, 255], [230, 230, 230], [210, 210, 210]]}, True,
                                                    {"QTTY": "Quantity", "NAME": "Name", "EXT":
                                                     "Extension", "TRAINCAT": "Category"}, "NAME")
    objects["SORT MENU"] = Object_box.Interface_box(sort_menu, "Pokemon")

    ##                  ##
    ## FILTER INTERFACE ##
    ##                  ##

    objects["FILTER TOGGLE"] = User_interface.Check_box(Constant.FONT_FILE, [18, 18], [390, 156], 0,
                                                        [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                        {"TXT": "", "SIZE": 0,
                                                         "COLOR": [0, 0, 0]}, [250, 250, 250], 2, True)
    objects["FILTER MENU"] = User_interface.Button(Constant.FONT_FILE, [120, 40], [430, 145], 2,
                                                   [[134, 265, 239], [104, 135, 209], [84, 115, 179]],
                                                   {"TXT": "Filter", "SIZE": 20,
                                                    "COLOR": [[250, 250, 250], [230, 230, 230], [210, 210, 210]]}, True)

    ##               ##
    ## FILTER WINDOW ##
    ##               ##

    ##
    ## POKEMONS
    ##

    filter_objects["Pokemon"] = {}

    # LVL
    filter_objects["Pokemon"]["LVL BAND"] = User_interface.Txt_band([20, 70], Constant.FONT_FILE, "Level :", 22, [250, 250, 250])

    filter_objects["Pokemon"]["Basic"] = User_interface.Check_box(Constant.FONT_FILE, [18, 18], [30, 110], 2,
                                                                  [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                  {"TXT": "Basic", "SIZE": 20,
                                                                   "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Pokemon"]["Restored"] = User_interface.Check_box(Constant.FONT_FILE, [18, 18], [30, 150], 2,
                                                                     [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                     {"TXT": "Restored", "SIZE": 20,
                                                                      "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Pokemon"]["Stage 1"] = User_interface.Check_box(Constant.FONT_FILE, [18, 18], [30, 190], 2,
                                                                    [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                    {"TXT": "Stage 1", "SIZE": 20,
                                                                     "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Pokemon"]["Stage 2"] = User_interface.Check_box(Constant.FONT_FILE, [18, 18], [30, 230], 2,
                                                                    [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                    {"TXT": "Stage 2", "SIZE": 20,
                                                                     "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Pokemon"]["BREAK"] = User_interface.Check_box(Constant.FONT_FILE, [18, 18], [30, 270], 2,
                                                                  [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                  {"TXT": "BREAK", "SIZE": 20,
                                                                   "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Pokemon"]["Level X"] = User_interface.Check_box(Constant.FONT_FILE, [18, 18], [30, 310], 2,
                                                                    [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                    {"TXT": "Level X", "SIZE": 20,
                                                                     "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)

    # TYPE
    filter_objects["Pokemon"]["TYPE BAND"] = User_interface.Txt_band([300, 70], Constant.FONT_FILE, "Type :", 22, [250, 250, 250])
    filter_objects["Pokemon"]["IMG FIRE"] = User_interface.Image_obj([300, 120], Constant.TYPE_DIR + "fire.png")
    filter_objects["Pokemon"]["Fire"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [280, 125], 2,
                                                                 [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                 {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Pokemon"]["IMG COLORLESS"] = User_interface.Image_obj([300, 155], Constant.TYPE_DIR + "colorless.png")
    filter_objects["Pokemon"]["Colorless"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [280, 160], 2,
                                                                      [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                      {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Pokemon"]["IMG GRASS"] = User_interface.Image_obj([300, 190], Constant.TYPE_DIR + "grass.png")
    filter_objects["Pokemon"]["Grass"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [280, 195], 2,
                                                                  [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                  {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Pokemon"]["IMG METAL"] = User_interface.Image_obj([300, 225], Constant.TYPE_DIR + "metal.png")
    filter_objects["Pokemon"]["Metal"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [280, 230], 2,
                                                                  [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                  {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Pokemon"]["IMG FIGHTING"] = User_interface.Image_obj([375, 120], Constant.TYPE_DIR + "fighting.png")
    filter_objects["Pokemon"]["Fighting"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [355, 125], 2,
                                                                     [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                     {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Pokemon"]["IMG LIGHTNING"] = User_interface.Image_obj([375, 155], Constant.TYPE_DIR + "lightning.png")
    filter_objects["Pokemon"]["Lightning"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [355, 160], 2,
                                                                      [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                      {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Pokemon"]["IMG PSYCHIC"] = User_interface.Image_obj([375, 190], Constant.TYPE_DIR + "psychic.png")
    filter_objects["Pokemon"]["Psychic"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [355, 195], 2,
                                                                    [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                    {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Pokemon"]["IMG DARKNESS"] = User_interface.Image_obj([375, 225], Constant.TYPE_DIR + "darkness.png")
    filter_objects["Pokemon"]["Darkness"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [355, 230], 2,
                                                                     [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                     {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Pokemon"]["IMG FAIRY"] = User_interface.Image_obj([450, 120], Constant.TYPE_DIR + "fairy.png")
    filter_objects["Pokemon"]["Fairy"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [430, 125], 2,
                                                                  [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                  {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Pokemon"]["IMG WATER"] = User_interface.Image_obj([450, 155], Constant.TYPE_DIR + "water.png")
    filter_objects["Pokemon"]["Water"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [430, 160], 2,
                                                                  [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                  {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Pokemon"]["IMG DRAGON"] = User_interface.Image_obj([450, 190], Constant.TYPE_DIR + "dragon.png")
    filter_objects["Pokemon"]["Dragon"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [430, 195], 2,
                                                                   [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                   {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)

    # ATTRIBUTE
    filter_objects["Pokemon"]["ATTRIBUTE BAND"] = User_interface.Txt_band([600, 70], Constant.FONT_FILE, "Attribute :", 22, [250, 250, 250])

    filter_objects["Pokemon"]["EX"] = User_interface.Check_box(Constant.FONT_FILE, [18, 18], [620, 110], 2,
                                                               [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                               {"TXT": "EX", "SIZE": 20,
                                                                "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Pokemon"]["Mega"] = User_interface.Check_box(Constant.FONT_FILE, [18, 18], [620, 150], 2,
                                                                 [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                 {"TXT": "Mega", "SIZE": 20,
                                                                  "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Pokemon"]["Legend"] = User_interface.Check_box(Constant.FONT_FILE, [18, 18], [620, 190], 2,
                                                                   [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                   {"TXT": "Legend", "SIZE": 20,
                                                                    "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)

    ##
    ## ENERGY
    ##

    filter_objects["Energy"] = {}

    # CATEGORY
    filter_objects["Energy"]["CAT BAND"] = User_interface.Txt_band([20, 70], Constant.FONT_FILE, "Category :", 22, [250, 250, 250])

    filter_objects["Energy"]["Special"] = User_interface.Check_box(Constant.FONT_FILE, [18, 18], [30, 110], 2,
                                                                    [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                    {"TXT": "Special", "SIZE": 20,
                                                                     "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Energy"]["Basic"] = User_interface.Check_box(Constant.FONT_FILE, [18, 18], [30, 150], 2,
                                                                  [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                  {"TXT": "Basic", "SIZE": 20,
                                                                   "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)

    # TYPE
    filter_objects["Energy"]["TYPE BAND"] = User_interface.Txt_band([300, 70], Constant.FONT_FILE, "Type :", 22, [250, 250, 250])
    filter_objects["Energy"]["IMG FIRE"] = User_interface.Image_obj([300, 120], Constant.TYPE_DIR + "fire.png")
    filter_objects["Energy"]["Fire"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [280, 125], 2,
                                                                [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Energy"]["IMG COLORLESS"] = User_interface.Image_obj([300, 155], Constant.TYPE_DIR + "colorless.png")
    filter_objects["Energy"]["Colorless"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [280, 160], 2,
                                                                     [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                     {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Energy"]["IMG GRASS"] = User_interface.Image_obj([300, 190], Constant.TYPE_DIR + "grass.png")
    filter_objects["Energy"]["Grass"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [280, 195], 2,
                                                                 [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                 {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Energy"]["IMG METAL"] = User_interface.Image_obj([300, 225], Constant.TYPE_DIR + "metal.png")
    filter_objects["Energy"]["Metal"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [280, 230], 2,
                                                                 [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                 {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Energy"]["IMG FIGHTING"] = User_interface.Image_obj([375, 120], Constant.TYPE_DIR + "fighting.png")
    filter_objects["Energy"]["Fighting"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [355, 125], 2,
                                                                    [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                    {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Energy"]["IMG LIGHTNING"] = User_interface.Image_obj([375, 155], Constant.TYPE_DIR + "lightning.png")
    filter_objects["Energy"]["Lightning"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [355, 160], 2,
                                                                     [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                     {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Energy"]["IMG PSYCHIC"] = User_interface.Image_obj([375, 190], Constant.TYPE_DIR + "psychic.png")
    filter_objects["Energy"]["Psychic"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [355, 195], 2,
                                                                   [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                   {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Energy"]["IMG DARKNESS"] = User_interface.Image_obj([375, 225], Constant.TYPE_DIR + "darkness.png")
    filter_objects["Energy"]["Darkness"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [355, 230], 2,
                                                                    [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                    {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Energy"]["IMG FAIRY"] = User_interface.Image_obj([450, 120], Constant.TYPE_DIR + "fairy.png")
    filter_objects["Energy"]["Fairy"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [430, 125], 2,
                                                                 [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                 {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Energy"]["IMG WATER"] = User_interface.Image_obj([450, 155], Constant.TYPE_DIR + "water.png")
    filter_objects["Energy"]["Water"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [430, 160], 2,
                                                                 [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                 {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Energy"]["IMG DRAGON"] = User_interface.Image_obj([450, 190], Constant.TYPE_DIR + "dragon.png")
    filter_objects["Energy"]["Dragon"] = User_interface.Check_box(Constant.FONT_FILE, [14, 14], [430, 195], 2,
                                                                  [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                  {"TXT": "", "SIZE": 0, "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)

    ##
    ## TRAINER
    ##

    filter_objects["Trainer"] = {}

    # CATEGORY
    filter_objects["Trainer"]["CAT BAND"] = User_interface.Txt_band([20, 70], Constant.FONT_FILE, "Category :", 22, [250, 250, 250])

    filter_objects["Trainer"]["Supporter"] = User_interface.Check_box(Constant.FONT_FILE, [18, 18], [30, 110], 2,
                                                                      [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                      {"TXT": "Supporter", "SIZE": 20,
                                                                       "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Trainer"]["Tool"] = User_interface.Check_box(Constant.FONT_FILE, [18, 18], [30, 150], 2,
                                                                 [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                 {"TXT": "Tool", "SIZE": 20,
                                                                  "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Trainer"]["Item"] = User_interface.Check_box(Constant.FONT_FILE, [18, 18], [30, 190], 2,
                                                                 [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                 {"TXT": "Item", "SIZE": 20,
                                                                  "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Trainer"]["Stadium"] = User_interface.Check_box(Constant.FONT_FILE, [18, 18], [30, 230], 2,
                                                                    [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                    {"TXT": "Stadium", "SIZE": 20,
                                                                     "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)
    filter_objects["Trainer"]["Technical Machine"] = User_interface.Check_box(Constant.FONT_FILE, [18, 18], [30, 270], 2,
                                                                              [[250, 250, 250], [150, 150, 150], [50, 50, 50]],
                                                                              {"TXT": "Technical Machine", "SIZE": 20,
                                                                               "COLOR": [0, 0, 0]}, [250, 250, 250], 2, False)

    window_filter = User_interface.Window(Constant.FONT_FILE, [200, 100], [1200, 800],
                                          {"WIN": [177, 190, 215], "BAR": [157, 170, 225], "CLOSE": [[], [250, 60, 60], [220, 50, 50]]},
                                          {"TXT": "F i l t e r", "SIZE": 30, "COLOR": [250, 250, 250]}, filter_objects, "Pokemon")
    return objects, window_filter

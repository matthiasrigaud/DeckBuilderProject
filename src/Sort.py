##
## Contain function to sort cards
##

import functools
from . import Collection

## sort by extension (latest->oldest)
def by_extension(card_a, card_b):
    if card_a["EXTENSION"] != card_b["EXTENSION"]:
        return card_b["EXTENSION_DATA"][1] - card_a["EXTENSION_DATA"][1]
    return card_a["NUMBER"] - card_b["NUMBER"]

## sort by name (A->Z)
def by_name(card_a, card_b):
    name_b = card_b["NAME"].lower()
    name_a = card_a["NAME"].lower()

    for i in range(len(name_b)):
        if i == len(name_a):
            return -ord(name_b[i])
        if name_a[i] != name_b[i]:
            return (ord(name_a[i]) - ord(name_b[i]))
    if name_a != name_b:
        return ord(name_a[len(name_a) - 1])
    return by_extension(card_a, card_b)

## sort by stage (order managed by stage_dict)
def by_stage(card_a, card_b):
    stage_dict = {"Basic": 0,
                  "Restored": 1,
                  "Stage 1": 2,
                  "Stage 2": 3,
                  "BREAK": 4,
                  "Level X": 5}
    if card_a["SUBCATEGORY"] not in stage_dict:
        stage_a = -1
    else:
        stage_a = stage_dict[card_a["SUBCATEGORY"]]
    if card_b["SUBCATEGORY"] not in stage_dict:
        stage_b = -1
    else:
        stage_b = stage_dict[card_b["SUBCATEGORY"]]
    if stage_a - stage_b == 0:
        return by_name(card_a, card_b)
    return stage_a - stage_b

## sort by type (kinda the same that by_name, but use TYPE field instead of NAME)
def by_type(card_a, card_b):
    for i in range(len(card_b["TYPE"])):
        if i == len(card_a["TYPE"]):
            return by_name(card_a, card_b)
        if card_a["TYPE"][i] != card_b["TYPE"][i]:
            return (ord(card_a["TYPE"][i]) - ord(card_b["TYPE"][i]))
    return by_name(card_a, card_b)

## sort by quantity (nbr of copies for each card) : fewest -> greatest
def by_quantity(card_a, card_b):
    collection = Collection.Collection()
    if collection.get_nb_cards(card_a) - collection.get_nb_cards(card_b) == 0:
        return by_name(card_a, card_b)
    return collection.get_nb_cards(card_a) - collection.get_nb_cards(card_b)

## sort by category (energy) : order managed by category_dict
def by_energy_categories(card_a, card_b):
    category_dict = {"Basic": 0,
                     "Special": 1}
    if card_a["SUBCATEGORY"] not in category_dict:
        category_a = -1
    else:
        category_a = category_dict[card_a["SUBCATEGORY"]]
    if card_b["SUBCATEGORY"] not in category_dict:
        category_b = -1
    else:
        category_b = category_dict[card_b["SUBCATEGORY"]]
    if category_a - category_b == 0:
        return by_name(card_a, card_b)
    return category_a - category_b

## sort by category (trainer) : order managed by category_dict
def by_trainer_categories(card_a, card_b):
    category_dict = {"Supporter": 0,
                     "Tool": 1,
                     "Item": 2,
                     "Stadium": 3,
                     "Technical Machine": 4}
    if card_a["SUBCATEGORY"] not in category_dict:
        category_a = -1
    else:
        category_a = category_dict[card_a["SUBCATEGORY"]]
    if card_b["SUBCATEGORY"] not in category_dict:
        category_b = -1
    else:
        category_b = category_dict[card_b["SUBCATEGORY"]]
    if category_a - category_b == 0:
        return by_name(card_a, card_b)
    return category_a - category_b

## sort by evolution family (order by Basic pokemons name)
def by_family(card_a, card_b):
    if not "FROM" in card_a:
        name_a = card_a["NAME"].lower()
    else:
        name_a = card_a["FROM"].lower()
    if not "FROM" in card_b:
        name_b = card_b["NAME"].lower()
    else:
        name_b = card_b["FROM"].lower()
    for i in range(len(name_b)):
        if i == len(name_a):
            return -ord(name_b[i])
        if name_a[i] != name_b[i]:
            return (ord(name_a[i]) - ord(name_b[i]))
    if name_a != name_b:
        return ord(name_a[len(name_a) - 1])
    return by_stage(card_a, card_b)

def sort_cards(cards_data, criter):
    sort_dict = {"QTTY": by_quantity,
                 "NAME": by_name,
                 "STAGE": by_stage,
                 "TYPE": by_type,
                 "EXT": by_extension,
                 "TRAINCAT": by_trainer_categories,
                 "NRJCAT": by_energy_categories,
                 "FML": by_family}
    return sorted(cards_data, key=functools.cmp_to_key(sort_dict[criter]))

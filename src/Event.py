##
## EVENT MANAGER
##

import sys
import pygame

from . import Constant, User_interface

def manager(cursor, maxcursor, is_in_zoom, objects, cardfilter, nb_cards, sort_criter, draw, window_filter):
    have_filter_change = False
    new_sort_criter = None
    ##
    ## browse pygame event stack
    ##
    for event in pygame.event.get():

        ## Quit when cross or echap is press
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.dict["key"] == pygame.K_ESCAPE):
            sys.exit()

        ##
        ## if window filter is opened, event are manage in a different way that if window filter is not opened
        ##
        if window_filter.is_opened():

            ## if an object is overflew, return it and its name
            overflew_object, name_object = window_filter.get_interactions()

            ## if left button is pressed, do action on current overflew object
            if event.type == pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0]:
                draw = True
                if name_object == "CLOSE":
                    window_filter.set_close()
                elif overflew_object:
                    overflew_object.toggle()
                    objects["FILTER TOGGLE"].set_state(0)
                    window_filter.change_filter(cardfilter)
                    have_filter_change = True
        else:

            ## make the cursor go backward, forward, or directly to the end or the begin of the cards array
            if (is_in_zoom == False and ((event.type == pygame.MOUSEBUTTONDOWN and event.dict["button"] == 4) or
                                         (objects["BACKWARD"].is_overflew() and pygame.mouse.get_pressed()[0]) or
                                         (event.type == pygame.KEYDOWN and event.dict["key"] == pygame.K_LEFT and
                                          objects["SEARCH BOX"].get_state() != 0))
                and cursor > 0 and pygame.mouse.get_pos()[1] > Constant.TOOLBAR_HEIGHT):
                cursor -= 1
            elif (is_in_zoom == False and ((event.type == pygame.MOUSEBUTTONDOWN and event.dict["button"] == 5) or
                                           (objects["FORWARD"].is_overflew() and pygame.mouse.get_pressed()[0]) or
                                           (event.type == pygame.KEYDOWN and event.dict["key"] == pygame.K_RIGHT and
                                            objects["SEARCH BOX"].get_state() != 0))
                  and cursor < maxcursor - 1 and pygame.mouse.get_pos()[1] > Constant.TOOLBAR_HEIGHT):
                cursor += 1
            elif (is_in_zoom == False and objects["TO BEGIN"].is_overflew() and pygame.mouse.get_pressed()[0]):
                cursor = 0
            elif (is_in_zoom == False and objects["TO END"].is_overflew() and pygame.mouse.get_pressed()[0]):
                cursor = maxcursor - 1

            ## if the event is a mouse button click, inactive special interface object (search box and sort menu)
            if (event.type == pygame.MOUSEBUTTONDOWN):
                objects["SEARCH BOX"].set_state(2)
                if objects["SORT MENU"].is_dropped() and not objects["SORT MENU"].is_overflew():
                    new_sort_criter = objects["SORT MENU"].change_active_element()
                    draw = True
            elif (event.type == pygame.KEYDOWN and objects["SEARCH BOX"].get_state() == 0):
                ## if the event is keydown and search box is selected, then write in it
                objects["SEARCH BOX"].edit_str(event.dict["key"])
                have_filter_change = True
                cardfilter["NAME"] = [objects["SEARCH BOX"].get_str()]

            ## make the card go in or quit the zoom mode
            if (event.type == pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[2]
                and pygame.mouse.get_pos()[0] > Constant.X_MIDDLE_CARD
                and pygame.mouse.get_pos()[1] > Constant.Y_MIDDLE_CARD
                and pygame.mouse.get_pos()[0] < Constant.X_MIDDLE_CARD + Constant.MIDDLE_CARD_WIDTH
                and pygame.mouse.get_pos()[1] < Constant.Y_MIDDLE_CARD + Constant.MIDDLE_CARD_HEIGHT
                and not is_in_zoom):
                is_in_zoom = True
            elif (is_in_zoom and event.type == pygame.MOUSEBUTTONDOWN and
                  (pygame.mouse.get_pressed()[2] or pygame.mouse.get_pressed()[0])):
                is_in_zoom = False
            elif not is_in_zoom:

                ## if the card is already unzoomed, browse interface objects to get interactions
                for i in iter(objects):

                    ## look the object state. If object isn't overflew by mouse, nothing can happen
                    if objects[i].is_overflew() :

                        ## if object is overflew and event is a left click, do associated action
                        if event.type == pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0]:

                            if i == "Pokemon" or i == "Trainer" or i == "Energy":
                                ## change sub-tab
                                objects["Pokemon"].set_state(2)
                                objects["Trainer"].set_state(2)
                                objects["Energy"].set_state(2)
                                objects[i].set_state(0)
                                cardfilter["CATEGORY"] = [i]
                                have_filter_change = True
                            elif i == "PLUS":
                                ## increase cur_card number of copies
                                nb_cards += 1
                                if cardfilter["NOT OWNED"] and not cardfilter["OWNED"]:
                                    have_filter_change = True
                            elif i == "MINUS" and nb_cards > 0:
                                ## decrease cur_card number of copies
                                nb_cards -= 1
                                if not cardfilter["NOT OWNED"] and cardfilter["OWNED"]:
                                    have_filter_change = True
                            elif i == "OWNED" or i == "NOT OWNED":
                                ## toggle owned and not owned checkboxes
                                objects[i].toggle()
                                have_filter_change = True
                                cardfilter[i] = (not cardfilter[i])
                            elif i == "FILTER TOGGLE":
                                ## toggle filter
                                objects[i].toggle()
                                have_filter_change = True
                                if objects[i].get_state() != 0:
                                    for k in range(len(Constant.FILTER_CATEGORIES)):
                                        if Constant.FILTER_CATEGORIES[k] in cardfilter:
                                            del cardfilter[Constant.FILTER_CATEGORIES[k]]
                                else:
                                    window_filter.change_filter(cardfilter)
                            elif i == "SEARCH BOX":
                                ## active search box
                                objects[i].set_state(0)
                            elif i == "SORT MENU":
                                ## change sort criter
                                new_sort_criter = objects[i].change_active_element()
                            elif i == "FILTER MENU":
                                ## open window filter
                                window_filter.set_open()
                            draw = True
                        elif objects[i].get_state() != 0:
                            ## if object is overflew but no click happen, give it the overflew state
                            objects[i].set_state(1)
                    elif objects[i].get_state() != 0:
                        ## if object isn't overflew, give it the inactive state
                        objects[i].set_state(2)
    if new_sort_criter:
        sort_criter = new_sort_criter
    return (cursor, is_in_zoom, cardfilter, have_filter_change, nb_cards, sort_criter, draw)

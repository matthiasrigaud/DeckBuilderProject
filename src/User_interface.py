##
## User interface class definitions
##

import pygame

from . import Constant

##
## Base class for interface object
##
class Interface_object(object):

    def __init__(self, path, size, position, state, color, txt, in_toolbar):
        self.path = path
        self.size = size
        self.pos = position
        self.state = state
        self.color = color
        self.txt = txt["TXT"]
        self.txt_size = txt["SIZE"]
        self.txt_color = txt["COLOR"]
        self.in_toolbar = in_toolbar
        self.origin = [0, 0]

    ##
    ## state :
    ## * 0 -> active
    ## * 1 -> overflew
    ## * 2 -> inactive
    ##

    def set_state(self, new_state):
        self.state = new_state

    def get_state(self):
        return self.state

    def is_in_toolbar(self):
        return self.in_toolbar

    def set_origin(self, origin):
        self.pos = [self.pos[0] - self.origin[0] + origin[0], self.pos[1] - self.origin[1] + origin[1]]
        self.origin = origin

    def is_overflew(self):
        if (pygame.mouse.get_pos()[0] > self.pos[0]
            and pygame.mouse.get_pos()[1] > self.pos[1]
            and pygame.mouse.get_pos()[0] < self.pos[0] + self.size[0]
            and pygame.mouse.get_pos()[1] < self.pos[1] + self.size[1]):
            return True
        return False

##
## Window object
##
class Window(object):

    def __init__(self, path, position, size, colors, txt, objects, mode):
        self.path = path
        self.mode = mode
        self.pos = position
        self.size = size
        self.win_color = colors["WIN"]
        self.bar_color = colors["BAR"]
        self.txt = txt["TXT"]
        self.txt_size = txt["SIZE"]
        self.txt_color = txt["COLOR"]
        self.bar_size = [size[0], int(txt["SIZE"] * 1.5)]
        for cur_mode in iter(objects):
            for cur_object in iter(objects[cur_mode]):
                objects[cur_mode][cur_object].set_origin(self.pos)
        self.objects = objects
        self.close_button = Button(path, [self.bar_size[1], self.bar_size[1]],
                                   [position[0] + size[0] - self.bar_size[1], position[1]], 2, colors["CLOSE"],
                                   {"TXT": "X", "SIZE": txt["SIZE"], "COLOR": [[], [250, 250, 250], [150, 150, 150]]}, False)
        self.opened = False

    def is_opened(self):
        return self.opened

    def change_mode(self, new_mode):
        self.mode = new_mode

    def set_open(self):
        self.opened = True

    def set_close(self):
        self.opened = False

    def change_filter(self, cardfilter):
        for i in range(len(Constant.FILTER_CATEGORIES)):
            if Constant.FILTER_CATEGORIES[i] in cardfilter:
                del cardfilter[Constant.FILTER_CATEGORIES[i]]
        for i in iter(self.objects[self.mode]):
            if self.objects[self.mode][i].get_state() == 0:
                if not Constant.WIN_FILTER_VALUES[i] in cardfilter:
                    cardfilter[Constant.WIN_FILTER_VALUES[i]] = []
                cardfilter[Constant.WIN_FILTER_VALUES[i]].append(i)

    def get_interactions(self):
        object_to_send = None
        name_object_to_send = None
        if self.opened:
            for i in iter(self.objects[self.mode]):
                if self.objects[self.mode][i].is_overflew():
                    if self.objects[self.mode][i].get_state() != 0:
                        self.objects[self.mode][i].set_state(1)
                    object_to_send = self.objects[self.mode][i]
                    name_object_to_send = i
                elif self.objects[self.mode][i].get_state() != 0:
                    self.objects[self.mode][i].set_state(2)
            if self.close_button.is_overflew():
                self.close_button.set_state(1)
                object_to_send = self.close_button
                name_object_to_send = "CLOSE"
            else:
                self.close_button.set_state(2)
        return object_to_send, name_object_to_send

    def draw(self, screen):
        if self.opened:
            font = pygame.font.Font(self.path, self.txt_size)
            text_surface = font.render(self.txt, True, self.txt_color)
            pygame.draw.rect(screen, self.win_color, pygame.Rect(self.pos, self.size))
            pygame.draw.rect(screen, self.bar_color, pygame.Rect(self.pos, self.bar_size))
            screen.blit(text_surface, [self.pos[0] + (self.bar_size[0] - text_surface.get_size()[0]) / 2,
                                       self.pos[1] + (self.bar_size[1] - text_surface.get_size()[1]) / 2])
            self.close_button.draw(screen)
            for cur_object in iter(self.objects[self.mode]):
                self.objects[self.mode][cur_object].draw(screen)

##
## Text box object (allow to take text from user)
##
class Text_box(Interface_object):

    def __init__(self, path, size, position, state, color, txt, in_toolbar):
        super(Text_box, self).__init__(path, size, position, state, color, txt, in_toolbar)
        self.current_txt = ""
        self.cursor = 0
        self.x_txt = self.pos[0] + self.txt_size / 4

    def draw(self, screen):
        text_to_print = []
        if len(self.current_txt) == 0 and self.state != 0:
            text_to_print.append(self.txt)
        else:
            if self.state == 0:
                if len(self.current_txt[:self.cursor]) > 0:
                    text_to_print.append(self.current_txt[:self.cursor])
                text_to_print.append("|")
                if self.cursor < len(self.current_txt):
                    text_to_print.append(self.current_txt[self.cursor:])
            else:
                text_to_print.append(self.current_txt)
        font = pygame.font.Font(self.path, self.txt_size)
        text_surface = []
        for i in range(0, len(text_to_print)):
            text_surface.append(font.render(text_to_print[i], True, self.txt_color[self.state]))
        pygame.draw.rect(screen, self.color[self.state], pygame.Rect(self.pos, self.size))
        if len(text_surface) == 2:
            if text_surface[0].get_size()[0] + text_surface[1].get_size()[0] > self.size[0] - self.txt_size / 4 and text_to_print[1] == "|":
                self.x_txt = self.pos[0] + self.txt_size / 4 - (text_surface[0].get_size()[0] +
                                                                text_surface[1].get_size()[0] * 2 - self.size[0])
            else:
                self.x_txt = self.pos[0] + self.txt_size / 4
        elif len(text_surface) == 3:
            if self.x_txt + text_surface[0].get_size()[0] < self.pos[0] + self.txt_size / 4:
                self.x_txt += self.pos[0] + self.txt_size / 4 - (self.x_txt + text_surface[0].get_size()[0])
            elif self.x_txt + text_surface[0].get_size()[0] + text_surface[1].get_size()[0] > self.pos[0] + self.size[0]:
                self.x_txt -= self.x_txt + text_surface[0].get_size()[0] + text_surface[1].get_size()[0] - (self.pos[0] + self.size[0])
        text_pos = [self.x_txt, self.pos[1] + (self.size[1] - text_surface[0].get_size()[1]) / 2]
        screen.set_clip(pygame.Rect(self.pos, self.size))
        for i in range(0, len(text_surface)):
            screen.blit(text_surface[i], text_pos)
            text_pos[0] += text_surface[i].get_size()[0]
        screen.set_clip()

    def get_str(self):
        return self.current_txt

    def edit_str(self, key):
        if key == pygame.K_BACKSPACE and self.cursor > 0:
            if (self.cursor == 1):
                tmp_txt = self.current_txt[1:]
            else:
                tmp_txt = self.current_txt[:(self.cursor - 1)]
                tmp_txt = tmp_txt + self.current_txt[(self.cursor):]
            self.current_txt = tmp_txt
            self.cursor -= 1
        elif key == pygame.K_RIGHT and self.cursor < len(self.current_txt):
            self.cursor += 1
        elif key == pygame.K_LEFT and self.cursor > 0:
            self.cursor -= 1
        elif key in Constant.KEY_CHAR:
            if self.cursor > 0:
                tmp_txt = self.current_txt[:self.cursor] + Constant.KEY_CHAR[key]
                if self.cursor < len(self.current_txt):
                    tmp_txt = tmp_txt + self.current_txt[self.cursor:]
            else:
                tmp_txt = Constant.KEY_CHAR[key] + self.current_txt
            self.current_txt = tmp_txt
            self.cursor += 1

##
## Button object
##
class Button(Interface_object):

    def __init__(self, path, size, position, state, color, txt, in_toolbar):
        super(Button, self).__init__(path, size, position, state, color, txt, in_toolbar)

    def draw(self, screen):
        font = pygame.font.Font(self.path, self.txt_size)
        text_surface = font.render(self.txt, True, self.txt_color[self.state])
        pygame.draw.rect(screen, self.color[self.state], pygame.Rect(self.pos, self.size))
        screen.blit(text_surface, [self.pos[0] + (self.size[0] - text_surface.get_size()[0]) / 2,
                                   self.pos[1] + (self.size[1] - text_surface.get_size()[1]) / 2])

class Drop_menu(Interface_object):

    def __init__(self, path, size, position, state, color, txt, in_toolbar, content_list, active_element):
        super(Drop_menu, self).__init__(path, size, position, state, color, txt, in_toolbar)
        self.dropped = False
        self.content_list = content_list
        self.button_list = {}
        y_button = position[1]
        for i in iter(content_list):
            y_button += size[1]
            txt["TXT"] = content_list[i]
            self.button_list[i] = Button(path, size, [position[0], y_button], 2, color, txt, False)
            if i == active_element:
                self.button_list[i].set_state(0)

    def is_dropped(self):
        return self.dropped

    def get_active_element(self):
        for i in iter(self.button_list):
            if self.button_list[i].get_state() == 0:
                return i

    def change_active_element(self):
        if not self.dropped:
            self.dropped = True
            return None
        else:
            self.dropped = False
            for i in iter(self.button_list):
                if self.button_list[i].is_overflew():
                    for j in iter(self.button_list):
                        self.button_list[j].set_state(2)
                    self.button_list[i].set_state(0)
                    return i
        return None

    def is_overflew(self):
        if self.dropped:
            for i in iter(self.button_list):
                if self.button_list[i].is_overflew() and self.button_list[i].get_state() != 0:
                    self.button_list[i].set_state(1)
                elif self.button_list[i].get_state() != 0:
                    self.button_list[i].set_state(2)
        return super(Drop_menu, self).is_overflew()

    def draw(self, screen):
        font = pygame.font.Font(self.path, self.txt_size)
        text_surface = font.render(self.txt, True, self.txt_color[0])
        screen.blit(text_surface, [self.pos[0] + (self.size[0] - text_surface.get_size()[0]) / 2,
                                   self.pos[1] - text_surface.get_size()[1] - self.txt_size / 4])
        if not self.dropped:
            for i in iter(self.button_list):
                if self.button_list[i].get_state() == 0:
                    current_txt = self.content_list[i]
            text_surface = font.render(current_txt, True, self.txt_color[self.state])
            pygame.draw.rect(screen, self.color[self.state], pygame.Rect(self.pos, self.size))
            screen.blit(text_surface, [self.pos[0] + (self.size[0] - text_surface.get_size()[0]) / 2,
                                       self.pos[1] + (self.size[1] - text_surface.get_size()[1]) / 2])
        else:
            for i in iter(self.button_list):
                self.button_list[i].draw(screen)
            pygame.draw.rect(screen, self.color[self.state], pygame.Rect(self.pos, self.size))
            pygame.draw.aalines(screen, self.txt_color[self.state], True,
                                [[int(self.pos[0] + self.size[0] - self.size[1] / 2),
                                  int(self.pos[1] + self.size[1] * 0.8)],
                                 [self.pos[0] + self.size[0], int(self.pos[1] + self.size[1] * 0.2)],
                                 [self.pos[0] + self.size[0] - self.size[1], int(self.pos[1] + self.size[1] * 0.2)]], True)

##
## Check box object (switch)
##
class Check_box(Interface_object):

    def __init__(self, path, size, position, state, color, txt, tick, width, in_toolbar):
        super(Check_box, self).__init__(path, size, position, state, color, txt, in_toolbar)
        self.tick = tick
        self.width = width

    def toggle(self):
        if self.state == 0:
            self.state = 1
        else:
            self.state = 0

    def draw(self, screen):
        font = pygame.font.Font(self.path, self.txt_size)
        text_surface = font.render(self.txt, True, self.txt_color)
        screen.blit(text_surface, [int(self.pos[0] + self.size[0] + self.txt_size / 2), self.pos[1]])
        if self.state == 0:
            pygame.draw.aaline(screen, self.tick,
                               [self.pos[0], self.pos[1]],
                               [self.pos[0] + self.size[0],
                                self.pos[1] + self.size[1]], True)
            pygame.draw.aaline(screen, self.tick,
                               [self.pos[0] + self.size[0], self.pos[1]],
                               [self.pos[0], self.pos[1] + self.size[1]], True)
        pygame.draw.rect(screen, self.color[self.state], pygame.Rect(self.pos, self.size), self.width)

##
## Text band object (to display text)
##
class Txt_band(Interface_object):

    def __init__(self, pos, font_path, txt, size, color):
        self.pos = pos
        self.font_path = font_path
        self.txt = txt
        self.size = size
        self.color = color
        self.origin = [0, 0]

    def draw(self, screen):
        font = pygame.font.Font(self.font_path, self.size)
        text_surface = font.render(self.txt, True, self.color)
        screen.blit(text_surface, self.pos)

    def get_state(self):
        return 2

    def is_overflew(self):
        return False

##
## Image object (to display image)
##
class Image_obj(Interface_object):

    def __init__(self, pos, path):
        self.pos = pos
        self.image = pygame.image.load(path)
        self.origin = [0, 0]

    def draw(self, screen):
        screen.blit(self.image, self.pos)

    def get_state(self):
        return 2

    def is_overflew(self):
        return False

##
## This file contain some graphical function
##

import pygame

from src import Constant, User_interface

## display cards in the scrolling zone
## cursor refer to the middle card
def scrolled_cards_displayer(screen, cards, cursor, is_in_zoom):
    scroll_cards = []
    i = 0

    if cursor - 2 >= 0:
        scroll_cards.append({});
        scroll_cards[i]["SCAN"] = pygame.image.load(Constant.DB_DIR + "/" + cards[cursor - 2]["EXTENSION"] + "/" + cards[cursor - 2]["SCAN"]);
        scroll_cards[i]["SCAN"].set_alpha(Constant.EDGE_CARD_ALPHA)
        scroll_cards[i]["SIZE"] = [int(Constant.MIDDLE_CARD_WIDTH * Constant.EDGE_CARD_PERCENT_SIZE),
                                   int(Constant.MIDDLE_CARD_HEIGHT * Constant.EDGE_CARD_PERCENT_SIZE)]
        scroll_cards[i]["POS"] = [int(Constant.X_MIDDLE_CARD * (1 - Constant.EDGE_CARD_PERCENT_X)),
                                  int(Constant.Y_MIDDLE_CARD * (2 - Constant.EDGE_CARD_PERCENT_SIZE))]
        i += 1
    if cursor + 2 < len(cards):
        scroll_cards.append({});
        scroll_cards[i]["SCAN"] = pygame.image.load(Constant.DB_DIR + "/" + cards[cursor + 2]["EXTENSION"] + "/" + cards[cursor + 2]["SCAN"]);
        scroll_cards[i]["SCAN"].set_alpha(Constant.EDGE_CARD_ALPHA)
        scroll_cards[i]["SIZE"] = [int(Constant.MIDDLE_CARD_WIDTH * Constant.EDGE_CARD_PERCENT_SIZE),
                                   int(Constant.MIDDLE_CARD_HEIGHT * Constant.EDGE_CARD_PERCENT_SIZE)]
        scroll_cards[i]["POS"] = [int(Constant.MIDDLE_CARD_WIDTH * (1 - Constant.EDGE_CARD_PERCENT_SIZE)
                                      + Constant.X_MIDDLE_CARD * (1 + Constant.EDGE_CARD_PERCENT_X)),
                                  int(Constant.Y_MIDDLE_CARD * (2 - Constant.EDGE_CARD_PERCENT_SIZE))]
        i += 1
    if cursor - 1 >= 0:
        scroll_cards.append({});
        scroll_cards[i]["SCAN"] = pygame.image.load(Constant.DB_DIR + "/" + cards[cursor - 1]["EXTENSION"] + "/" + cards[cursor - 1]["SCAN"]);
        scroll_cards[i]["SCAN"].set_alpha(Constant.NEXT_CARD_ALPHA)
        scroll_cards[i]["SIZE"] = [int(Constant.MIDDLE_CARD_WIDTH * Constant.NEXT_CARD_PERCENT_SIZE),
                                   int(Constant.MIDDLE_CARD_HEIGHT * Constant.NEXT_CARD_PERCENT_SIZE)]
        scroll_cards[i]["POS"] = [int(Constant.X_MIDDLE_CARD * (1 - Constant.NEXT_CARD_PERCENT_X)),
                                  int(Constant.Y_MIDDLE_CARD * (2 - Constant.NEXT_CARD_PERCENT_SIZE))]
        i += 1
    if cursor + 1 < len(cards):
        scroll_cards.append({});
        scroll_cards[i]["SCAN"] = pygame.image.load(Constant.DB_DIR + "/" + cards[cursor + 1]["EXTENSION"] + "/" + cards[cursor + 1]["SCAN"]);
        scroll_cards[i]["SCAN"].set_alpha(Constant.NEXT_CARD_ALPHA)
        scroll_cards[i]["SIZE"] = [int(Constant.MIDDLE_CARD_WIDTH * Constant.NEXT_CARD_PERCENT_SIZE),
                                   int(Constant.MIDDLE_CARD_HEIGHT * Constant.NEXT_CARD_PERCENT_SIZE)]
        scroll_cards[i]["POS"] = [int(Constant.MIDDLE_CARD_WIDTH * (1 - Constant.NEXT_CARD_PERCENT_SIZE)
                                      + Constant.X_MIDDLE_CARD * (1 + Constant.NEXT_CARD_PERCENT_X)),
                                  int(Constant.Y_MIDDLE_CARD * (2 - Constant.NEXT_CARD_PERCENT_SIZE))]
        i += 1
    if len(cards) > 0:
        scroll_cards.append({});
        scroll_cards[i]["SCAN"] = pygame.image.load(Constant.DB_DIR + "/" + cards[cursor]["EXTENSION"] + "/" + cards[cursor]["SCAN"]);
        if is_in_zoom:
            scroll_cards[i]["SIZE"] = scroll_cards[i]["SCAN"].get_size()
            scroll_cards[i]["POS"] = [Constant.X_MIDDLE_CARD - (scroll_cards[i]["SIZE"][0] - Constant.MIDDLE_CARD_WIDTH) / 2,
                                      (Constant.WIN_HEIGHT - scroll_cards[i]["SIZE"][1]) / 2]
        else:
            scroll_cards[i]["SIZE"] = [Constant.MIDDLE_CARD_WIDTH, Constant.MIDDLE_CARD_HEIGHT]
            scroll_cards[i]["POS"] = [Constant.X_MIDDLE_CARD, Constant.Y_MIDDLE_CARD]
        i += 1
    for j in range(0, i):
        if scroll_cards[j]["SCAN"].get_size()[0] > scroll_cards[j]["SCAN"].get_size()[1] and (not is_in_zoom or j != i - 1):
            scroll_cards[j]["SCAN"] = pygame.transform.rotate(scroll_cards[j]["SCAN"], -90)
        screen.blit(pygame.transform.scale(scroll_cards[j]["SCAN"], scroll_cards[j]["SIZE"]), scroll_cards[j]["POS"]);

##
## draw user interface object
##
def draw_toolbar(screen, objects):
    pygame.draw.rect(screen, Constant.TOOLBAR_COLOR, pygame.Rect([0, 0], [Constant.TOOLBAR_WIDTH, Constant.TOOLBAR_HEIGHT]))
    for i in iter(objects):
        if objects[i].is_in_toolbar():
            objects[i].draw(screen)

##
## draw card interface
##
def draw_interface(screen, plus, minus, nb_cards):
    str_nb = str(nb_cards)
    size = 25
    plus.draw(screen)
    minus.draw(screen)
    if nb_cards > 9999:
        str_nb = "Too much"
        size = 15
    font = pygame.font.Font(Constant.FONT_FILE, size)
    text_surface = font.render(str_nb, True, [255, 255, 255])
    x_displayer = Constant.X_MIDDLE_CARD + Constant.MIDDLE_CARD_WIDTH / 2 - 40
    y_displayer = Constant.Y_MIDDLE_CARD + Constant.MIDDLE_CARD_HEIGHT - 20
    pygame.draw.rect(screen, [0, 0, 150], pygame.Rect([x_displayer, y_displayer], [80, 40]))
    screen.blit(text_surface, [x_displayer + (80 - text_surface.get_size()[0]) / 2,
                               y_displayer + (40 - text_surface.get_size()[1]) / 2])

#!/bin/sh

##
## Installation script for Pokemon DeckBuilder
##

TMPFILE="/tmp/tmpfile$$"

FINAL_NAME="pkmnDeckBuilder"

FINAL_SRC_DIR="/usr/lib/${FINAL_NAME}/"
FINAL_DATA_DIR="/usr/share/${FINAL_NAME}/"
FINAL_CMD="/usr/bin/${FINAL_NAME}"

CUR_SRC_DIR="./src/"
CUR_DATA_DIR="./TCG-DataBase/"

CONST_FILE_NAME="Constant.py"

CONST_FILE="${CUR_SRC_DIR}/${CONST_FILE_NAME}"
MAIN_FILE="./DeckBuilder.py"

sudo mkdir -p ${FINAL_SRC_DIR}
sudo mkdir -p ${FINAL_DATA_DIR}

sudo cp -Rf ${CUR_DATA_DIR}/* ${FINAL_DATA_DIR}

cat ${MAIN_FILE} | sudo sed -e "s%('.')%('${FINAL_SRC_DIR}')%g" > "${TMPFILE}"

sudo mv "${TMPFILE}" "${FINAL_CMD}"
sudo chmod +x "${FINAL_CMD}"

sudo cp -Rf ${CUR_SRC_DIR} ${FINAL_SRC_DIR}

cat ${CONST_FILE} | sudo sed -e "s%${CUR_DATA_DIR}%${FINAL_DATA_DIR}%g" > "${TMPFILE}"

sudo mv "${TMPFILE}" "${FINAL_SRC_DIR}/src/${CONST_FILE_NAME}"
